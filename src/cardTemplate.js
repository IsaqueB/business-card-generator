module.exports = function (style, params){
    return (
        `<!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>Document</title>
            </head>
            <style>
                ${style}
            </style>
            <body>
                <div class="mainContainer">
                    <div class="header">
                        <label>${params.header}</label>
                    </div>
                    <div class="profileImage">
                        <img src="${params.profilePic}" alt="profileImageTag"/>
                    </div>
                    <div class="info">
                        <label class="profileName">
                            <label>${params.name}</label>
                        </label>
                        <label class="profileEmail">
                            <label>${params.email}</label>
                        </label>
                        <label class="profileContact">
                            <label>${params.contact}</label>
                        </label>
                    </div>
                    <div class="linksContainer">
                        <div class="link" id="link1">
                            <img src="https://img.icons8.com/ios/452/contacts.png" alt="profileImageTag1"/>
                        </div>
                        <div class="link" id="link2">
                            <img src="https://i.pinimg.com/originals/00/50/71/005071cbf1fdd17673607ecd7b7e88f6.png" alt="profileImageTag2"/>
                        </div>
                        <div class="link" id="link3">
                            <img src="https://www.iconpacks.net/icons/1/free-mail-icon-142-thumb.png" alt="profileImageTag3"/>
                        </div>
                    </div>
                </div>
            </body>
        </html>`)
}