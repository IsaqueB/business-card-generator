const puppeteer = require('puppeteer')
const fs = require('fs')
const createHTML = require('./cardTemplate')

module.exports = async (infoFromRequest) => {
    const htmlForPuppeteer = await fillHtmlTemplate(infoFromRequest)
    const pageFormat = {
        width: infoFromRequest.format.width,
        height: infoFromRequest.format.height
    }
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport(pageFormat)
    await page.setContent("text/html; charset=UTF-8")
    await page.goto(`data:text/html,${htmlForPuppeteer}`, {waitUntil:'networkidle0'});
    const image = await page.screenshot({type: 'png'})
    const paramsForLink = await page.$$eval('.link', setParamsForLinks,["http://br.linkedin.com","http://www.unifor.br","http://www.google.com"])
    await browser.close();
    return({
        params: paramsForLink,
        image: image,
        screenshotFormat: pageFormat
    })
}

async function fillHtmlTemplate(infoFromRequest){
    return new Promise(function(resolve, reject){
        fs.readFile("../stylesheet/cartaoDeVisita.css", 'utf8', async function(err, stylesheet){
            if(err){reject(err)}
            resolve(createHTML(stylesheet, infoFromRequest))
        })
    })
}
function setParamsForLinks(aTags, links){
    let paramsForLinkObject = []
    for(let index = 0; index < aTags.length; index++){
        paramsForLinkObject.push({
            left: aTags[index].offsetLeft, 
            top: aTags[index].offsetTop, 
            width: aTags[index].offsetWidth, 
            height: aTags[index].offsetHeight,
            url: links[index]
        })
    }
    return paramsForLinkObject
}