const jspdf = require('jspdf')
const browserScreenshot = require('./browserScreenshot')

module.exports = {
    pdfGenerator: async function(req, res){
        let screenshotParams = await browserScreenshot(req.body)
        const doc = new jspdf.jsPDF({
            orientation: 'portrait',
            hotfixes: ["px_scaling"],
            unit: 'px',
            format: [screenshotParams.screenshotFormat.width, screenshotParams.screenshotFormat.height]
        });
        doc.addImage(screenshotParams.image, 'JPEG', 0, 0, screenshotParams.screenshotFormat.width, screenshotParams.screenshotFormat.height, 'profilePic', 'NONE', 0)
        addLinksToPdf(doc, screenshotParams.params)
        res.type('pdf').send(new Buffer.from(doc.output('arraybuffer'), 'base64'))
    }
}

function addLinksToPdf(instanceOfDoc, paramsForLinks){
        for(let index = 0; index < paramsForLinks.length; index++){
            instanceOfDoc.link(
                paramsForLinks[index].left, paramsForLinks[index].top, 
                paramsForLinks[index].width, paramsForLinks[index].height, 
                {url: paramsForLinks[index].url}
            )
        }
}